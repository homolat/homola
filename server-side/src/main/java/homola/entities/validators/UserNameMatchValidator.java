package homola.entities.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserNameMatchValidator implements ConstraintValidator<UserNameMatch, String> {

  public void initialize(UserNameMatch constraintAnnotation) {}

  public boolean isValid(String value,  ConstraintValidatorContext context) {
    if(value == null) {
	return false;
    }

    if(value.contains("@")) {
      return false;
    }    

    return true; 

  }


}
