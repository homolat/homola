package homola.entities.validators;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = UserNameMatchValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserNameMatch {
  String message() default "Fields values don't match!";
  Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
