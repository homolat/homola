package homola.entities;

import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size; 
import javax.validation.constraints.NotNull;

@Entity
@Table(name="roles")
public class Role {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    @Column(length=128)
    @Size(min=1, max=128)
    @NotNull
    private String name;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_users")
    private User user;

    public Role() {}

    public Role(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Role(String name) {
        this.name = name;
    }
    public Role(String name, User user) {
        this.name = name;
        this.user = user;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Role{" + "id=" + id + ", name=" + name + ", user=" + user + '}';
    }
   
    
}
