package homola.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.Size; 
import javax.validation.constraints.NotNull;
import homola.entities.validators.UserNameMatch;
import homola.entities.views.UserView;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
@Table(name="Users")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    @Column(unique=true, length=128)
    @Size(min=1, max=128)
    @NotNull
    @UserNameMatch
    private String name;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Role> roles;

    public User() {}

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

   public User(UserView user) {
     this.id = user.getId();
     this.name = user.getName();
   }  
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Role> getRoles() {
        return roles;
    }
    
    

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", roles=" + roles + '}';
    }  
}
