package homola.entities.views;

import homola.entities.User;
import homola.entities.validators.UserNameMatch;
import java.util.Objects;

public class UserView {
    private Integer id;
    @UserNameMatch
    private String name;

    public UserView() {
    }
 

    public UserView(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

   public UserView(User user) {
     this.id = user.getId();
     this.name = user.getName();
   }

    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserView{" + "id=" + id + ", name=" + name + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserView other = (UserView) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
