package homola.repositories;

import homola.entities.User;
import java.lang.Integer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Integer> {
}
