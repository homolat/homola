package homola;

import org.springframework.beans.factory.annotation.Autowired;
import homola.services.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.NullRememberMeServices;
import org.springframework.security.web.authentication.RememberMeServices;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  final static Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

    @Autowired
    DataSource dataSource;
   
    @Autowired 
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        logger.debug("configure: ["+auth+"]");
        auth
                .jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery(
                       "select name as username,'1234' as password, 'true' as enabled from users where name=?")
                .authoritiesByUsernameQuery(
                        "select (select name from users u where u.id=r.id_users) as username"
                       +", name as role from roles as r where id_users=(select id from users where name=?)")
                ;
    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    @Override
    protected void configure(HttpSecurity http) throws Exception {
	  logger.debug("configure: ["+http+"]");
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/secured/**").hasRole("USER").anyRequest().permitAll()
                 .and().formLogin();
    }
    
}
