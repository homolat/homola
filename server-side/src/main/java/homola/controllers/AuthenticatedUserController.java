package homola.controllers;

import homola.entities.views.UserView;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.MediaType;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping(value = "/secured/auth/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticatedUserController {

    final static Logger logger = LoggerFactory.getLogger(AuthenticatedUserController.class);

    public AuthenticatedUserController() {}

    @Transactional
    @GetMapping
    public UserView getUser(@AuthenticationPrincipal UserDetails user) {
        return new UserView(-1, user.getUsername());
    }

}
