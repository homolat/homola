package homola.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.MediaType;
import homola.services.UsersService;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;

@RestController
@RequestMapping(value = "/secured/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class SecuredUsersController {

    final static Logger logger = LoggerFactory.getLogger(SecuredUsersController.class);

    private UsersService usersService;

    public SecuredUsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @Transactional
    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@AuthenticationPrincipal UserDetails user,
            @PathVariable("id") int id) {
        try {
            usersService.delete(id, user);
        } catch (InsufficientAuthenticationException ignore) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
