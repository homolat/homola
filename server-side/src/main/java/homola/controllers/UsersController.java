package homola.controllers;

import homola.entities.User;
import homola.entities.views.UserView;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import homola.services.UsersService;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.Valid;



@RestController
@RequestMapping(value="/users", produces=MediaType.APPLICATION_JSON_VALUE)
public class UsersController {
     final static Logger logger = LoggerFactory.getLogger(UsersController.class);

    private UsersService usersService;

    public UsersController(UsersService usersService) {
     this.usersService = usersService;
    }

    @GetMapping
    public List<UserView> getUsers() {
         logger.debug("getUsers");
	return usersService.getUsers();
    }

    @GetMapping("/{id}") 
    public ResponseEntity<UserView> getUserById(@PathVariable("id") int id) {
	Optional<UserView> rs =  usersService.getUserById(id);
        if(rs.isPresent())
          return new ResponseEntity<UserView>(rs.get(), HttpStatus.OK);

        logger.warn("getUserById: id: ["+id+" cant be founded");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @Transactional
    @RequestMapping(method={RequestMethod.PUT, RequestMethod.POST})    
    public UserView saveOrUpdateUser(@Valid @RequestBody UserView user) {
      return usersService.saveOrUpdateUser(user);   
  }

   @DeleteMapping
    public String delete() {
      return "redirect: /secured/users";
   }

}
