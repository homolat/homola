package homola.services;

import homola.entities.Role;
import javax.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.List;
import java.util.Arrays;
import homola.entities.User;
import homola.entities.views.UserView;
import homola.repositories.UsersRepository;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.transaction.Transactional;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

@Service
@Transactional
public class UsersService {

    final static Logger logger = LoggerFactory.getLogger(UsersService.class);

    private UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public Optional<UserView> getUserById(int userId) {
        logger.debug("getUserById: [" + userId + "]");
        try {
            User u = usersRepository.getOne(userId);
            return Optional.of(new UserView(u));
        } catch (EntityNotFoundException e) {
            return Optional.empty();
        }
    }

    public List<UserView> getUsers() {
        return usersRepository.findAll().stream().map((dbUser) -> {
            return new UserView(dbUser); 
        }).collect(Collectors.toList());
    }

    public UserView saveOrUpdateUser(UserView user) {
        logger.debug("saveOrUpdateUser: [" + user + "]");
        User dbUser = new User(user);
        dbUser.setRoles(Arrays.asList(new Role("ROLE_USER",dbUser)));
        return new UserView(usersRepository.saveAndFlush(dbUser));
    }

    /**
     * Delete user based on id If currently logged user has admin role delete
     * user otherwise allow to delete only currently logged user
     *
     * @param id
     * @param userDetails
     * 
     * @throws InsufficientAuthenticationException if currently logged user is trying to delete different user.
     */
    @Transactional(dontRollbackOn = InsufficientAuthenticationException.class)
    public void delete(int id, UserDetails userDetails) {
        logger.debug("delete: [id=" + id + "], user=[" + userDetails + "]");
        if (userDetails.getAuthorities().stream().anyMatch((a) -> {
            return "ROLE_ADMIN".equals(a.getAuthority());
        })) {
            logger.debug("User: ["+userDetails.getUsername()+"] has admin role, allow to delete");
            usersRepository.deleteInBatch(Arrays.asList(usersRepository.getOne(id)));
        } else {
            try {
                User user = usersRepository.getOne(id);
                if (user.getName().equals(userDetails.getUsername())) {
                    usersRepository.delete(usersRepository.getOne(id));
		    usersRepository.flush();
                } else {
                    throw new InsufficientAuthenticationException("User: ["+userDetails.getUsername()+"] is not is not allowed to delte "
                            + "user: ["+user.getName()+"]");
                }
            } catch (EntityNotFoundException ignore) {
                // ignore
            }
        }
    }
}
