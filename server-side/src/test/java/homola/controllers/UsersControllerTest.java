package homola.controllers;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import homola.services.UsersService;
import homola.entities.User;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import homola.entities.views.UserView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UsersController.class, secure = false)
public class UsersControllerTest {
  final static Logger logger = LoggerFactory.getLogger(UsersControllerTest.class);

  @Autowired 
  private ObjectMapper jsonObjectMapper;
  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private UsersService usersService;

  private static List<UserView> USERS;
  @BeforeClass 
  public static void beforeClass() {
     USERS = Arrays.asList(new UserView(1, "John Doe"), new UserView(2, "Test"));
    
  }
  @Test
  public void testGetUsers_returnAllUsersAsArray() throws Exception {
    Mockito.when(usersService.getUsers()).thenReturn(USERS);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/users").accept(
				MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    assertEquals(result.getResponse().getStatus(), HttpServletResponse.SC_OK);
    List<Map> jsonUserList = jsonObjectMapper.readValue(result.getResponse().getContentAsString(), List.class);
    assertEquals("Expected 2 users in list", jsonUserList.size(), 2);
    int index = 0;
    for(Map m : jsonUserList)  {
      assertThat(USERS.get(index), is(jsonObjectMapper.convertValue(m, UserView.class)));
      index++;
    }
  }

  @Test
  public void testGetUser_UserExists_returnUser() throws Exception {
    Mockito.when(usersService.getUserById(1)).thenReturn(Optional.of(USERS.get(0)));
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                                "/users/1").accept(
                                MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpServletResponse.SC_OK);
    UserView user = jsonObjectMapper.readValue(result.getResponse().getContentAsString(), UserView.class);
    assertEquals("User: "+USERS.get(0)+" expected", user, USERS.get(0));
  }

  @Test
  public void testGetUser_UserNotExists_returnHttp404() throws Exception {
    Mockito.when(usersService.getUserById(1)).thenReturn(Optional.empty());
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                                "/users/1").accept(
                                MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpServletResponse.SC_NOT_FOUND);
  }

  @Test
  public void saveOrUpdateUser_postValidUser_userSaved() throws Exception {
    saveUser(HttpMethod.POST);
  }

  @Test
  public void saveOrUpdateUser_putValidUser_userSaved() throws Exception {
    saveUser(HttpMethod.PUT);
  }

  @Test
  public void saveOrUpdateUser_userHasInvalidValues_returnValidatonError() throws Exception {
    UserView user = USERS.get(0);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/users")
                                 .content("{\"id\": "+user.getId()+", \"name\": \"John Doe @\"}")
                                 .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpServletResponse.SC_BAD_REQUEST);
  }

  public void saveUser(HttpMethod method) throws Exception {
    UserView user = USERS.get(0);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.request(method, "/users")
                                 .content("{\"id\": "+user.getId()+", \"name\": \""+user.getName()+"\"}")
                                 .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpServletResponse.SC_OK);
    Mockito.verify(usersService, Mockito.times(1)).saveOrUpdateUser(user);
  }

}
