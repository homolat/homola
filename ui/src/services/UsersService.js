import request from 'superagent';
import User from '../entities/User';


class UsersService {
    constructor() {
        this.getUsers = this.getUsers.bind(this);
    }
    getUsers(callback) {
        request.get('/rest/users')
                .set('Accept', 'application/json')
                .end(function (err, res) {
                    if (res.ok) {
                        let users = [];
                        res.body.forEach(j => {
                            users.push(new User(j.id, j.name));
                        });
                        callback(users);
                    }
                });
    }
    addUser(user, callback) {
        request.post("/rest/users")
                .set('Accept', 'application/json')
                .send({"name": user.name})
                .end((err, res) => {
                    if (res.ok) {
                        callback(new User(res.body.id, res.body.name));
                    } else {
                        window.alert(res.body.exception);
                    }
                }
                );

    }
    removeUser(user, callback) {
        request.delete("rest/secured/users/" + user.id)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (res.status === 200) {
                        callback(user);
                    } else {
                        callback(res.status);
                    }
                });
    }

}



export default UsersService;
