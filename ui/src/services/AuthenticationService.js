import request from 'superagent';
import User from '../entities/User';


class AuthenticationService {
    /**
     * 
     * @param {type} username
     * @param {type} password
     * @returns {User} 
     */
    authenticateUser(username, password, callback) {
        console.log("Auth service: " + username);
        request.post("/rest/login")
                .set('Accept', 'Application/json')
                .redirects(1)
                .send("username=" + username)
                .send("password=" + password)
                .set('X-API-Key', 'foobar')
                .end((err, res) => {
                    request.get("/rest/secured/auth/user")
                            .set('Accept', 'Application/json')
                            .end((err, res) => {
                                if (res && res.ok) {
                                    window.user = new User(res.body.id, res.body.name);
                                    callback(window.user);
                                } else {
                                    callback();
                                }
                            })
                });

    }
    removeUserAuthenticatedCallback(callback) {
        if (window.authCallbacks) {
            let copy = [];
            window.authCallbacks.forEach(c => {
                if (c !== callback)
                    copy.push(c);
            });
            window.authCallbacks = copy;
        }

    }

    registerUserAuthenticatedCallback(callback) {
        if (window.authCallbacks) {
            window.authCallbacks.push(callback);
        } else {
            window.authCallbacks = [];
            window.authCallbacks.push(callback);
        }
    }
    isUserAuthenticated() {
        return window.user ? true : false;
    }
    getAuthenticatedUser() {
        return window.user;
    }
    logout() {
        let that = this;
        request.get("/rest/logout")
                .end((err, res) => {
                    delete window.user;
                    that.informAboutAuthenticatedUser();
                });
    }
    informAboutAuthenticatedUser() {
        if (window.authCallbacks) {
            window.authCallbacks.forEach(c => {
                c(window.user);
            })
        }
    }

}


export default AuthenticationService;
