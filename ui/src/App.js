import React, { Component } from 'react';
import UsersTable from './components/UsersTable';
import LoggedUserInfo from './components/LoggedUserInfo';
import logo from './img/logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
          <LoggedUserInfo />    
        </header>
        <UsersTable x='a' />
      </div>
    );
  };
};

export default App;
