import React, { Component } from 'react';
import ModalDialog from './ModalDialog';
import UsersService  from './../services/UsersService';
import User from '../entities/User';
import './AddUser.css';

class AddUser extends Component {
    constructor(props) {
        super(props);
        this.usersService = new UsersService();
        this.state = {visible: true};

        this.onCancel = this.onCancel.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.clearInputs = this.clearInputs.bind(this);


    }

    componentWillReceiveProps(nextProps) {
        this.setState({visible: true});
    }
    
    onCancel() {
        this.setState({visible: false});

        this.clearInputs();
    }
    onAdd() {
        this.setState({visible: false});
        let username = document.getElementById("add_username").value;
        this.usersService.addUser(new User(-1, username), this.props.callback);
        this.clearInputs();
    }

    clearInputs() {
        const fields = ['add_username'];
        fields.forEach(field => {
            document.getElementById(field).value = "";
        });
    }
    render() {
        console.log("render: " + this.state.visible);
        let loginError, body, actions;
        body = (<div>
            {loginError}
            <label for="add_username">Username: </label>
            <input id="add_username" key="add_username" name="username" type="text"  />
        
        </div>);
        actions = [
            {value: 'Cancel', callback: this.onCancel},
            {value: 'Add', callback: this.onAdd}

        ];
        return (<ModalDialog key="addUser" visible={this.state.visible} header="Add user" actions={actions} body={body} />);
    }
}

export default AddUser;
