import React, { Component } from 'react';
import ReactDOM  from 'react-dom';
import './ModalDialog.css';

class ModalDialog extends Component {
    render() {
        let modalDialogClassName = this.props.visible && this.props.visible ? "ModalDialog" : "ModalDialog is-hidden";
        
        let buttons = [];
            this.props.actions.forEach((a, index) => {
                let className = "ModalDialog_action" + (index + 1);
                buttons[index] = <input type="button" className={className} key={index} onClick={a.callback} value={a.value} />
            });
            let modalDialog = (
                    <div className={modalDialogClassName}>
                        <div className="ModalDialog_content">
                            <div className="ModalDialog_header"><div className="ModalDialog_headerText">{this.props.header}</div></div>
                            <div className="ModalDialog_body"> 
                              <div className="ModalDialog_bodyContent">    
			    	{this.props.body}
			      </div>
                            </div> 
                            <div className="ModalDialog_actions">
			      <div className="ModalDialog_actionsContent">
                                 {buttons}
  			      </div>	
                            </div>
                        </div>
                    </div>
                    );
            ReactDOM.render(modalDialog, document.getElementById('modalDialog'));

        
        return null;
    }
}

export default ModalDialog;
