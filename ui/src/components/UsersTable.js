import React, { Component } from 'react';
import UsersService  from './../services/UsersService';
import AuthenticationService  from './../services/AuthenticationService';
import UserLogin from './UserLogin';
import AddUser from './AddUser';
import './UsersTable.css';
class UsersTable extends Component {
    constructor(props) {
        super(props);
        this.usersService = new UsersService();
        this.authenticationService = new AuthenticationService();
        this.addUser = this.addUser.bind(this);
        this.removeUser = this.removeUser.bind(this);
        this.userAdded = this.userAdded.bind(this);
        this.getUsers = this.getUsers.bind(this);
        this.usersService.getUsers(this.getUsers);
        this.state = {users: []}
    }
    init() {

    }
    getUsers(users) {
        this.setState({users: users, addUser: false, forceLogin: false});
    }
    userAdded(user) {
        this.usersService.getUsers(this.getUsers);
    }
    removeUser(user) {
        if (this.authenticationService.isUserAuthenticated()) {
            let that = this;
            this.usersService.removeUser(user, p => {
                console.log("removeUser: " + p);
                if (isNaN(p)) {
                    that.usersService.getUsers(that.getUsers);
                } else {
                    if (p === 403) {
                        window.alert("No rights to remove user: " + user.name);
                    } else {
                        throw p;
                    }
                }
            });

        } else {
            let callback = (u) => {
                this.removeUser(user);
                this.authenticationService.removeUserAuthenticatedCallback(callback);
            };
            this.authenticationService.registerUserAuthenticatedCallback(callback);
            this.setState({
                users: this.state.users,
                forceLogin: true,
                addUser: false
            });
        }
    }
    addUser(e) {
        this.setState({addUser: true,
            forceLogin: false});
    }
    render() {
        let userLogin, addUser;
        if (this.state.forceLogin) {
            userLogin = <UserLogin />;
        }
        if (this.state.addUser) {
            addUser = <AddUser callback={this.userAdded} />
        }
        return (
                <div className="UsersTable">
                    <div className="TableRow TableHeader">
                        <div className="TableRow1_2 TableHeader_Text">Name:</div>
                        <div className="TableRow2_2"><div className="TableHeader_AddUser" onClick={this.addUser}></div>
                        </div>
                    </div>
                    {
                        this.state.users.map((u, key) => {
                            return (<div key={key} className="TableRow">
                                <div className="TableRow1_2 TableRow_Text">{u.name}</div>
                                <div className="TableRow2_2"> 
                                    <div id="user_{u.id}" className="TableRow_Delete" onClick={() => this.removeUser(u)}></div>
                                </div>  
                            </div>);
                    }
                    )}
                    {userLogin}
                    {addUser}
                </div>
                    );
    }
}

export default UsersTable;
