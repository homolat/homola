import React, { Component } from 'react';
import ModalDialog from './ModalDialog';
import AuthenticationService  from './../services/AuthenticationService';
import './UserLogin.css';

class UserLogin extends Component {
    constructor(props) {
        super(props);
        this.user = props.user;
        this.authenticationService = new AuthenticationService();
        this.state = {visible: true, loginError: false};

        this.onCancel = this.onCancel.bind(this);
        this.onLogin = this.onLogin.bind(this);
        this.onUserAuthenticated = this.onUserAuthenticated.bind(this);
        this.clearInputs = this.clearInputs.bind(this);


    }

    componentWillReceiveProps(nextProps) {
        this.setState({visible: true, loginError: false});
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.authenticationService.isUserAuthenticated())
            this.authenticationService.informAboutAuthenticatedUser();
    }
    onCancel() {
        this.setState({visible: false, loginError: false});

        this.clearInputs();
    }
    onUserAuthenticated(user) {
        if (user && user !== null) {
            this.setState({visible: false, loginError: false});
        } else {
            this.setState({visible: true,
                loginError: true});
        }
    }
    onLogin() {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        this.authenticationService.authenticateUser(username, password, this.onUserAuthenticated);

        this.clearInputs();
    }

    clearInputs() {
        const fields = ['username', 'password'];
        fields.forEach(field => {
            document.getElementById(field).value = "";
        });
    }
    render() {
        console.log("render: " + this.state.visible);
        let loginError, body, actions;
        if (this.state.loginError) {
            loginError = <div>Invalid credentials</div>;
        }
        body = (<div>
            {loginError}
	    <label for="username">Username: </label>
            <input id="username" key="username" name="username" type="text"  />
            <label for="password">Password: </label>
            <input id="password" key="pasword" name="password" type="password" />
        
        </div>);
        actions = [
            {value: 'Cancel', callback: this.onCancel},
            {value: 'Login', callback: this.onLogin}

        ];
        return (<ModalDialog key="userLogin" visible={this.state.visible} header="Login" actions={actions} body={body} />);
    }
}

export default UserLogin;
