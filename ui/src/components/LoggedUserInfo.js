import React, { Component } from 'react';
import AuthenticationService from './../services/AuthenticationService';
import './LoggedUserInfo.css';

class LoggedUserInfo extends Component {
    constructor(props) {
        super(props);
        this.onUserAuthenticated = this.onUserAuthenticated.bind(this);
	this.logout = this.logout.bind(this);
        this.a = new AuthenticationService();
        if(this.a.isUserAuthenticated()) {
            this.state = {user: this.a.getAuthenticatedUser()};
        } else {
            this.state = {};
            this.a.registerUserAuthenticatedCallback(this.onUserAuthenticated);
        }
    }
    onUserAuthenticated(u) {
        this.setState({user: u});
    }
   logout() {
	this.a.logout();
    }
    render() {
        if(this.state.user) {
            return (
                <div className="LoggedUserInfo">
		<div className="LoggedUserInfo_username">Logged user: '{this.state.user.name}'</div>
		<div className="LoggedUserInfo_logout" onClick={this.logout}>Logout</div>
                </div>
		)  ;
        } else {
        return (<div></div>);
    }
    }
}

export default LoggedUserInfo;
